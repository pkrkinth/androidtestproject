import pytest
import urllib3
from appium import webdriver as appiumdriver

urllib3.disable_warnings()


@pytest.fixture
def data_center(request):
    return request.config.getoption('--dc')


sauce = False


@pytest.fixture
def android_driver():
    if sauce:

        username = 'P3077294',
        accessKey = 'b0f4a0f8-1da0-41c0-86fb-2ea917359f6a'

        username_cap = username
        access_key_cap = accessKey

        sauce_caps = {
            'username': 'username_cap',
            'accessKey': 'access_key_cap',
            'deviceName': 'Samsung_Galaxy_A31_real_us',
            'platformName': 'Android',
            'app': '6532fb00-588d-40ad-996c-aea4f2cb5dc5',
            'noReset': True,
            'fullReset': False,
            'newCommandTimeout': 90,
        }

        # sauce_url = 'https://' + username_cap + ':' + access_key_cap + '@ondemand.us-west-1.saucelabs.com:443/wd/hub'
        sauce_url = 'https://P3077294:b0f4a0f8-1da0-41c0-86fb-2ea917359f6a@ondemand.us-west-1.saucelabs.com:443/wd/hub'
        driver = appiumdriver.Remote(sauce_url, desired_capabilities = sauce_caps)
        yield driver
        driver.quit()
    else:
        caps = {
            "platformName": "Android",
            "deviceName": "emulator-5554",
            "app": "/Users/prince/Documents/Prince/AndroidTestProject/app/android-test-app.apk",
            "automationName": "UiAutomator2"
        }
        driver = appiumdriver.Remote("http://127.0.0.1:4723/wd/hub", caps)
        yield driver
        driver.quit()
