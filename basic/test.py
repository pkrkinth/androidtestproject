from appium.webdriver.common.mobileby import MobileBy
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time


def test(android_driver):
    # btn1 = android_driver.find_element(MobileBy.ID, value="com.example.androiddevproject:id/btn1")
    btn1 = WebDriverWait(android_driver, 30).until(
        EC.element_to_be_clickable((MobileBy.ID, "com.example.androiddevproject:id/btn1"))
    )
    btn1.click()
    # btn2 = android_driver.find_element(MobileBy.ID, value="com.example.androiddevproject:id/btn2")
    btn2 = WebDriverWait(android_driver, 30).until(
        EC.element_to_be_clickable((MobileBy.ID, "com.example.androiddevproject:id/btn2"))
    )
    btn2.click()